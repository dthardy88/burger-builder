import React, {Component} from 'react';
import Layout from './Components/Layouts/Layout';
import BurgerBuilder from './Containers/BurgerBuilder/BurgerBuilder';
import Checkout from './Containers/Checkout/Checkout';
import Orders from './Containers/Orders/Orders';
import {
	Route, 
	Switch
} from 'react-router-dom';

function App() {
  return (
    <div>
      <Layout>
      <Switch>
        <Route path="/" exact component={BurgerBuilder} />
        <Route path="/orders" exact component={Orders} />
	      <Route path="/checkout" component={Checkout} />
	    </Switch>
      </Layout>
    </div>
  );
}

export default App;
