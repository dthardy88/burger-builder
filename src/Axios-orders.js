import axios from 'axios';

const instance = axios.create({
	baseURL: 'https://react-burger-app-6966e.firebaseio.com'
});

export default instance;