import React from 'react';
import classes from './Burger.module.css';
import BurgerIngredient from './BurgerIngredient/BurgerIngredient';

const Burger = props => {

	// Convert object to array
	let transformedIngredients = Object.keys(props.ingredients).map(ingredient => {
		// Array() method creates empty array elements based on the number of ingredients
		return [...Array(props.ingredients[ingredient])].map((_, i) => {
			return <BurgerIngredient key={ingredient + i} type={ingredient} />
		});
		// Combine the 4 seperate arrays
	}).reduce((prevValue, currValue) => {
		return prevValue.concat(currValue);
	}, []);
	if(transformedIngredients.length === 0) {
		transformedIngredients = <p>Please start adding ingredients</p>
	}
	return (
		<div className={classes.burger}>
			<BurgerIngredient type="bread-top" />
			{transformedIngredients}
			<BurgerIngredient type="bread-bottom" />
		</div>
	);
}

export default Burger;