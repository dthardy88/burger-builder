import React from 'react';
import BurgerLogo from '../../Assets/Images/burger-logo.png';
import classes from './Logo.module.css';

const Logo = props => {
	return (
		<div className={classes.logo}>
			<img src={BurgerLogo} alt="MyBurger Logo" />
		</div>
	);
}

export default Logo;