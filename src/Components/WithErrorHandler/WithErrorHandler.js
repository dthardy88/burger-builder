import React, {Component} from 'react';
import Modal from '../UI/Modal/Modal';

const WithErrorHandler = (WrappedComponent, axios) => {
	return class extends Component {

		state = {
			error: false
		}

		errorConfirmedHandler = () => {
			this.setState({
				error: false
			});
		}

		componentWillMount() {
			this.reqInterceptor = axios.interceptors.request.use(req => {
				this.setState({
					error: null
				});
				return req;
			});
			this.resInterceptor = axios.interceptors.response.use(res => res, error => {
				this.setState({
					error: error
				});
			});
		}

		componentWillUnmount() {
			axios.interceptors.request.eject(this.reqInterceptor);
			axios.interceptors.request.eject(this.resInterceptor);
		}

		render() {
			return (
				<React.Fragment>
					<Modal 
					show={this.state.error}
					close={this.errorConfirmedHandler}>
						{this.state.error ? this.state.error.message : null}
					</Modal>
					<WrappedComponent {...this.props} />
				</React.Fragment>
			);
		}
	}
}

export default WithErrorHandler;