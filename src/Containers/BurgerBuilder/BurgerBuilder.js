import React, {Component} from 'react';
import Burger from '../../Components/Burger/Burger';
import BuildControls from '../../Components/Burger/BuildControls/BuildControls';
import Modal from '../../Components/UI/Modal/Modal';
import Spinner from '../../Components/UI/Spinner/Spinner';
import OrderSummary from '../../Components/Burger/OrderSummary/OrderSummary';
import WithErrorHandler from '../../Components/WithErrorHandler/WithErrorHandler';
import axios from '../../Axios-orders';

const INGREDIENT_PRICES = {
	salad: 0.5,
	cheese: 0.4,
	meat: 1.3,
	bacon: 0.7
}

class BurgerBuilder extends Component {

	state = {
		ingredients: null,
		totalPrice: 4.20,
		purchasable: true,
		purchaseMode: false,
		loading: false,
		error: false
	}

	componentDidMount() {
		axios.get('https://react-burger-app-6966e.firebaseio.com/Ingredients.json')
		.then(response => {
			this.setState({
				ingredients: response.data
			});
		})
		.catch(error => {
			this.setState({
				error: true
			});
		});
	}

	purchaseHandler = () => {
		this.setState({
			purchaseMode: true
		});
	}

	purchaseCancelHandler = () => {
		this.setState({
			purchaseMode: false
		});
	}

	updatePurchaseState = (ingredients) => {
		const sum = Object.keys(ingredients).map(name => {
			// Returns the specific ingredient value in the ingredients object
			return ingredients[name];
		}).reduce((sum, el) => {
			return sum + el;
		}, 0);
		this.setState({
			purchasable: sum > 0
		});
	}

	addIngredientHandler = type => {
		const oldCount = this.state.ingredients[type];
		const updatedCount = oldCount + 1;
		// Unpack the state into our object clone
		const updatedIngredients = {
			...this.state.ingredients
		}
		updatedIngredients[type] = updatedCount;
		const priceAddition = INGREDIENT_PRICES[type];
		const oldPrice = this.state.totalPrice;
		const newPrice = oldPrice + priceAddition;
		this.setState({
			totalPrice: newPrice,
			ingredients: updatedIngredients
		});
		this.updatePurchaseState(updatedIngredients);

	}

	removeIngredientHandler = type => {
		const oldCount = this.state.ingredients[type];
		// Prevent execution if there is no ingredients
		if(oldCount <= 0) {
			return;
		}
		const updatedCount = oldCount - 1;
		// Unpack the state into our object clone
		const updatedIngredients = {
			...this.state.ingredients
		}
		updatedIngredients[type] = updatedCount;
		const priceDeduction = INGREDIENT_PRICES[type];
		const oldPrice = this.state.totalPrice;
		const newPrice = oldPrice - priceDeduction;
		this.setState({
			totalPrice: newPrice,
			ingredients: updatedIngredients
		});
		this.updatePurchaseState(updatedIngredients);
	}

	purchaseContinueHandler = () => {
		const queryParams = [];
		queryParams.push('price='+this.state.totalPrice);
		for(let i in this.state.ingredients) {
			queryParams.push(encodeURIComponent(i) + '=' + encodeURIComponent(this.state.ingredients[i]));
		}
		const queryString = queryParams.join('&');
		this.props.history.push({
			pathname: '/checkout',
			search: '?'+queryString
		});
	}

	render() {
		const disabledInfo = {
			...this.state.ingredients
		};
		for(let key in disabledInfo) {
			disabledInfo[key] = disabledInfo[key] <= 0;
		}

		let burger = this.state.error ? <p>Ingredients cannot be loaded</p> : <Spinner />;
		let orderSummary = <Spinner />;

		if(this.state.ingredients) {
			burger = (
				<React.Fragment>
					<Burger ingredients={this.state.ingredients} />
					<BuildControls 
					ingredientAdded={this.addIngredientHandler}
					ingredientRemoved={this.removeIngredientHandler}
					disabled={disabledInfo}
					price={this.state.totalPrice}
					purchasable={this.state.purchasable}
					ordered={this.purchaseHandler} />
				</React.Fragment>
			);
			orderSummary = (<OrderSummary 
				ingredients={this.state.ingredients}
				purchaseCancelled={this.purchaseCancelHandler}
				purchaseContinue={this.purchaseContinueHandler}
				price={this.state.totalPrice} />);
		}

		if(this.state.loading) {
			orderSummary = <Spinner />
		}

		return (
			<React.Fragment>
				<Modal 
				show={this.state.purchaseMode}
				close={this.purchaseCancelHandler}>
					{orderSummary}
				</Modal>
				{burger}
			</React.Fragment>
		);
	}
}

export default WithErrorHandler(BurgerBuilder, axios);