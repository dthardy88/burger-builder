import React, {Component} from 'react';
import Button from '../../../Components/UI/Button/Button';
import classes from './ContactData.module.css';
import axios from 'axios';
import Spinner from '../../../Components/UI/Spinner/Spinner';
import Input from '../../../Components/Input/Input';

class ContactData extends Component {

	state = {
		orderForm: {
        	name: {
        		elementType: 'input',
        		elementConfig: {
        			type: 'text',
        			placeholder: 'Your Name'
        		},
        		value: '',
        		validation: {
        			required: true
        		},
        		valid: false,
        		touched: false
        	},
        	street: {
        		elementType: 'input',
        		elementConfig: {
        			type: 'text',
        			placeholder: 'Address line 1:'
        		},
        		value: '',
        		validation: {
        			required: true
        		},
        		valid: false,
        		touched: false
        	},
        	postcode: {
        		elementType: 'input',
        		elementConfig: {
        			type: 'text',
        			placeholder: 'Your postcode:'
        		},
        		value: '',
        		validation: {
        			required: true,
        			length: 7
        		},
        		valid: false,
        		touched: false
        	},
        	country: {
        		elementType: 'input',
        		elementConfig: {
        			type: 'text',
        			placeholder: 'Country:'
        		},
        		value: '',
        		validation: {
        			required: true
        		},
        		valid: false,
        		touched: false
        	},
        	email: {
        		elementType: 'input',
        		elementConfig: {
        			type: 'email',
        			placeholder: 'Your email:'
        		},
        		value: '',
        		validation: {
        			required: true
        		},
        		valid: false,
        		touched: false
        	},
        	deliveryMethod: {
        		elementType: 'select',
        		elementConfig: {
        			type: 'text',
        			options: [
        				{value: 'fastest', displayValue: 'Fastest'},
        				{value: 'cheapest', displayValue: 'Cheapest'}
        			]
        		},
        		value: 'fastest'
        	},
        },
		loading: false
	}

	orderHandler = (event) => {

		event.preventDefault();
		const formData = {};
		for(let formElementIdentifier in this.state.orderForm) {
			formData[formElementIdentifier] = this.state.orderForm[formElementIdentifier].value;
		}

        this.setState({
        	loading: true
        });

        const order = {
            ingredients: this.props.ingredients,
            price: this.props.price,
            orderData: formData
        }

        axios.post('https://react-burger-app-6966e.firebaseio.com/orders.json', order)
        	.then(response => {
                this.setState({
                	loading: false
                });
                this.props.history.push('/');
            })
            .catch(error => {
                this.setState({
                	loading:false
                });
            });

    }

    checkValidation = (value, rules) => {
    	let isValid = true;
    	if(rules.required) {
    		isValid = value.trim() !== '' && isValid;
    	}
    	if(rules.length) {
    		isValid = value.length == rules.length && isValid;
    	}
    	return isValid;
    }

    inputChangeHandler = (event, inputIdentifier) => {
    	const updatedOrderForm = {
    		...this.state.orderForm
    	};
    	const updatedFormElement = {
    		...updatedOrderForm[inputIdentifier]
    	};
    	updatedFormElement.value = event.target.value;
    	updatedFormElement.valid = this.checkValidation(updatedFormElement.value, updatedFormElement.validation);
    	updatedFormElement.touched = true;
    	updatedOrderForm[inputIdentifier] = updatedFormElement;
    	this.setState({
    		orderForm: updatedOrderForm
    	});
    }

	render() {
		const formElementsArray = [];
		for(let key in this.state.orderForm) {
			formElementsArray.push({
				id: key,
				config: this.state.orderForm[key]
			});
		}
		let form = (
			<form onSubmit={this.orderHandler}>
				{formElementsArray.map(formElement => {
					return(
						<Input 
						key={formElement.id}
						changed={(event) => {this.inputChangeHandler(event, formElement.id)}}
						elementType={formElement.config.elementType} 
						elementConfig={formElement.config.elementConfig} 
						invalid={!formElement.config.valid}
						value={formElement.config.value}
						touched={formElement.config.touched}
						shouldValidate={formElement.config.validation} />
					);
				})}
				<Button btnType="success">ORDER</Button>
			</form>
		);
		if(this.state.loading) {
			form = <Spinner />
		}
		return(
			<div className={classes.contactData}>
				<h4>Enter your Contact Data</h4>
				{form}
			</div>
		);
	}

}

export default ContactData;