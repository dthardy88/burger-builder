import React, {Component} from 'react';
import Order from '../../Components/Order/Order';
import axios from 'axios';
import WithErrorHandler from '../../Components/WithErrorHandler/WithErrorHandler';

class Orders extends Component {

	state = {
		orders: [],
		loading: true
	}

	componentDidMount() {
		axios.get('https://react-burger-app-6966e.firebaseio.com/orders.json')
		.then((response) => {
			let fetchedOrders = [];
			for(let key in response.data) {
				fetchedOrders.push({
					...response.data[key],
					id: key
				});
			}
			this.setState({
				loading:false,
				orders: fetchedOrders
			});
		}).catch(() => {
			this.setState({
				loading:false
			});
		});
	}

	render() {
		return(
			<div>
				{this.state.orders.map(order => {
					return(
						<Order 
						key={order.id}
						ingredients={order.ingredients}
						price={+order.price} />
					);
				})}
			</div>
		);
	}
}

export default WithErrorHandler(Orders, axios);